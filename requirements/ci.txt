-r base.txt
-e .
-e git+https://gitlab.com/kb/base.git#egg=kb-base
-e git+https://gitlab.com/kb/gdpr.git#egg=kb-gdpr
-e git+https://gitlab.com/kb/login.git#egg=kb-login
-e git+https://gitlab.com/kb/mail.git#egg=kb-mail
-e git+https://gitlab.com/kb/report.git#egg=kb-report
django-debug-toolbar
factory-boy
freezegun
pytest-cov
pytest-django
pytest-flakes
