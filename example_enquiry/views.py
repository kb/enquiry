# -*- encoding: utf-8 -*-
from django.urls import reverse
from django.views.generic import CreateView, TemplateView

from base.view_utils import BaseMixin
from enquiry.views import EnquiryCreateMixin


class EnquiryCreateView(EnquiryCreateMixin, BaseMixin, CreateView):
    """Save an enquiry in the database."""

    def get_success_url(self):
        return reverse("project.home")


class EnquiryMissingFormSettingsCreateView(
    EnquiryCreateMixin, BaseMixin, CreateView
):
    """Save an enquiry in the database."""

    consent_form_settings = "does-not-exist"

    def get_success_url(self):
        return reverse("project.home")


class HomeView(TemplateView):

    template_name = "example/home.html"


class SettingsView(TemplateView):

    template_name = "example/settings.html"
