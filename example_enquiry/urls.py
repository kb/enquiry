# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path, reverse_lazy
from django.views.generic import RedirectView

from .views import (
    EnquiryCreateView,
    EnquiryMissingFormSettingsCreateView,
    HomeView,
    SettingsView,
)


urlpatterns = [
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^", view=include("login.urls")),
    re_path(
        r"^add/$",
        view=EnquiryCreateView.as_view(),
        name="example.enquiry.create",
    ),
    re_path(
        r"^add/missing/form/settings/$",
        view=EnquiryMissingFormSettingsCreateView.as_view(),
        name="example.enquiry.missing.form.settings.create",
    ),
    re_path(r"^enquiry/", view=include("enquiry.urls")),
    re_path(r"^gdpr/", view=include("gdpr.urls")),
    re_path(
        r"^home/user/$",
        view=RedirectView.as_view(url=reverse_lazy("project.home")),
        name="project.dash",
    ),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#   ^ helper function to return a URL pattern for serving files in debug mode.
# https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user

urlpatterns += staticfiles_urlpatterns()
