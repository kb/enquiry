# -*- encoding: utf-8 -*-
import os
import pytest

from django.urls import reverse
from http import HTTPStatus

from enquiry.models import Enquiry, EnquiryError
from gdpr.models import UserConsent
from gdpr.tests.factories import ConsentFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from mail.management.commands import mail_send
from mail.models import Mail, Notify
from mail.tests.factories import NotifyFactory


def _post(client, user=None):
    if user is None:
        user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "consent_checkbox": True,
        "description": "Do you sell hay and straw?",
        "email": "richard@pkimber.net",
        "name": "Richard",
        "phone": "07840 538 357",
    }
    response = client.post(reverse("example.enquiry.create"), data)
    assert HTTPStatus.FOUND == response.status_code
    return response


@pytest.mark.django_db
def test_create(client):
    """check enquiry."""
    consent = ConsentFactory(
        slug=Enquiry.GDPR_CONTACT_SLUG,
        message="Your phone number...",
        show_checkbox=True,
    )
    user = UserFactory()
    _post(client, user)
    # check the enquiry was created
    Enquiry.objects.get(name="Richard")
    assert UserConsent.objects.consent_given(consent=consent, user=user) is True


@pytest.mark.django_db
def test_create_get_logged_in(client):
    """User is logged in - form should be pre-filled with the name and email."""
    ConsentFactory(
        slug=Enquiry.GDPR_CONTACT_SLUG,
        message="Your phone number...",
        show_checkbox=True,
    )
    user = UserFactory(
        first_name="P", last_name="Kimber", email="patrick@kbsoftware.co.uk"
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("example.enquiry.create"))
    # initial
    assert "form" in response.context
    form = response.context["form"]
    assert "email" in form.initial
    assert "name" in form.initial
    assert "patrick@kbsoftware.co.uk" == form.initial["email"]
    assert "P Kimber" == form.initial["name"]


@pytest.mark.django_db
def test_create_get_logged_in_not(client):
    """User is not logged in - form should not be filled with name and email."""
    ConsentFactory(
        slug=Enquiry.GDPR_CONTACT_SLUG,
        message="Your phone number...",
        show_checkbox=True,
    )
    response = client.get(reverse("example.enquiry.create"))
    # initial
    assert "form" in response.context
    form = response.context["form"]
    assert "email" not in form.initial
    assert "name" not in form.initial


@pytest.mark.django_db
def test_create_gdpr_checkbox(client):
    """If the consent checkbox is displayed, don't display the help text."""
    ConsentFactory(
        slug=Enquiry.GDPR_CONTACT_SLUG,
        message="Your phone number...",
        show_checkbox=True,
    )
    response = client.get(reverse("example.enquiry.create"))
    assert HTTPStatus.OK == response.status_code
    assert "consent_message" in response.context
    assert "" == response.context["consent_message"]
    assert "form" in response.context
    # The checkbox is visible and the label is set to the consent message
    form = response.context["form"]
    assert "consent_checkbox" in form.fields.keys()
    f = form.fields["consent_checkbox"]
    assert "Your phone number..." == f.label
    assert "" == f.help_text


@pytest.mark.django_db
def test_create_gdpr_checkbox_not_ticked(client):
    """If the consent checkbox is displayed, it should be ticked!"""
    ConsentFactory(
        slug=Enquiry.GDPR_CONTACT_SLUG,
        message="Your phone number...",
        no_consent_message="I am sorry",
        show_checkbox=True,
    )
    data = {
        "consent_checkbox": False,
        "description": "Please help",
        "captcha": "123",
        "g-recaptcha-response": "PASSED",
        "name": "Patrick",
        "phone": "07840 538 357",
    }
    response = client.post(reverse("example.enquiry.create"), data)
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert {"consent_checkbox": ["I am sorry"]} == form.errors


@pytest.mark.django_db
def test_create_gdpr_no_checkbox(client):
    """If the consent checkbox is not displayed, display the help text."""
    ConsentFactory(
        slug=Enquiry.GDPR_CONTACT_SLUG,
        message="Your phone number...",
        show_checkbox=False,
    )
    response = client.get(reverse("example.enquiry.create"))
    assert HTTPStatus.OK == response.status_code
    assert "consent_message" in response.context
    assert "Your phone number..." == response.context["consent_message"]
    assert "form" in response.context
    form = response.context["form"]
    assert "consent_checkbox" not in form.fields.keys()


@pytest.mark.django_db
def test_create_missing_form_settings(client):
    """check enquiry with missing form settings."""
    consent = ConsentFactory(
        slug=Enquiry.GDPR_CONTACT_SLUG,
        message="Your phone number...",
        show_checkbox=True,
    )
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with pytest.raises(EnquiryError) as e:
        client.get(reverse("example.enquiry.missing.form.settings.create"))
    assert "Cannot find consent form settings: 'does-not-exist'" in str(e.value)


@pytest.mark.django_db
def test_create_no_notify(client):
    """Check enquiry with no users to notify.

    Will write an error message to the log file, and won't send any
    emails.

    """
    ConsentFactory(slug=Enquiry.GDPR_CONTACT_SLUG)
    Notify.objects.all().delete()
    _post(client)
    Enquiry.objects.get(name="Richard")


@pytest.mark.django_db
def test_create_not_logged_in(client):
    """check enquiry."""
    ConsentFactory(
        slug=Enquiry.GDPR_CONTACT_SLUG,
        message="Your phone number...",
        show_checkbox=True,
    )
    data = {
        "name": "Richard",
        "consent_checkbox": True,
        "description": "Do you sell hay and straw?",
        "email": "richard@pkimber.net",
        "phone": "07840 538 357",
        "captcha": "123",
        "g-recaptcha-response": "PASSED",
    }
    response = client.post(reverse("example.enquiry.create"), data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    # check the enquiry was created
    enquiry = Enquiry.objects.get(name="Richard")
    # No user consent because the user is not logged in
    # https://www.kbsoftware.co.uk/crm/ticket/3268/
    assert 1 == UserConsent.objects.for_content_object(enquiry).count()


@pytest.mark.django_db
def test_enquiry_email(client):
    ConsentFactory(slug=Enquiry.GDPR_CONTACT_SLUG)
    NotifyFactory()
    _post(client)
    enquiry = Enquiry.objects.get(name="Richard")
    message = enquiry.message
    assert (
        "enquiry received from Richard, " "richard@pkimber.net on 07840 538 357"
    ) in message.description
    assert "Do you sell hay and straw?" in message.description
    assert "http://testserver/enquiry/" in message.description


@pytest.mark.django_db
def test_send_emails(client):
    """Test the management command."""
    ConsentFactory(slug=Enquiry.GDPR_CONTACT_SLUG)
    NotifyFactory()
    NotifyFactory()
    NotifyFactory()
    _post(client)
    enquiry = Enquiry.objects.get(name="Richard")
    message = enquiry.message
    pks = [m.pk for m in message.mail_set.all()]
    assert 3 == len(pks)
    for pk in pks:
        m = Mail.objects.get(pk=pk)
        m.sent = None
        m.sent_response_code = None
        m.save()
    # send the emails
    command = mail_send.Command()
    command.handle()
    # check the mail is marked as sent
    message = enquiry.message
    count = 0
    for m in message.mail_set.all():
        assert m.sent is not None
        count = count + 1
    assert 3 == count
