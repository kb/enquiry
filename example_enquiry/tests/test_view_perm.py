# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from enquiry.models import Enquiry
from enquiry.tests.factories import EnquiryFactory
from gdpr.tests.factories import ConsentFactory
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_list(perm_check):
    ConsentFactory(slug=Enquiry.GDPR_CONTACT_SLUG)
    url = reverse("example.enquiry.create")
    perm_check.anon(url)
