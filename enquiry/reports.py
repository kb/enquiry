# -*- encoding: utf-8 -*-
from datetime import date
from dateutil.relativedelta import relativedelta
from django.utils import timezone

from enquiry.models import Enquiry
from report.models import ReportError
from report.service import ReportMixin


class EnquiryMonthReport(ReportMixin):
    """Enquiries received.

    This report was originally called ``enquiry_month``.

    """

    REPORT_SLUG = "enquiry-month"
    REPORT_TITLE = "Enquiries received"

    def _check_parameters(self, parameters):
        if not parameters:
            raise ReportError(
                "Enquiries received needs year and month parameters"
            )
        if not "month" in parameters:
            raise ReportError("Enquiries received needs a month parameter")
        if not "year" in parameters:
            raise ReportError("Enquiries received needs a year parameter")
        month = int(parameters.get("month"))
        year = int(parameters.get("year"))
        return year, month

    def run_csv_report(self, csv_writer, parameters=None):
        count = 0
        csv_writer.writerow(
            ("created", "name", "description", "email", "phone")
        )
        year, month = self._check_parameters(parameters)
        d = date(year, month, 1)
        startdate = d + relativedelta(day=1)
        enddate = d + relativedelta(months=+1, day=1, days=-1)
        enquiries = Enquiry.objects.filter(
            created__gte=startdate, created__lte=enddate
        ).order_by("created")
        for x in enquiries:
            count = count + 1
            csv_writer.writerow(
                [
                    timezone.localtime(x.created).strftime("%d/%m/%Y %H:%M:%S"),
                    x.name,
                    x.description,
                    x.email,
                    x.phone,
                ]
            )
        return count
