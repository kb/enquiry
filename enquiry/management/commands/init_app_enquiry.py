# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from enquiry.models import Enquiry
from gdpr.models import Consent


class Command(BaseCommand):

    help = "Initialise 'enquiry' application"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        Consent.objects.init_consent(
            Enquiry.GDPR_CONTACT_SLUG,
            "Enquiries",
            "Enquiry (Contact) Form",
            True,
        )
        self.stdout.write("{} - Complete".format(self.help))
