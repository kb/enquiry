# -*- encoding: utf-8 -*-
from django.views.generic import ListView

from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.db import transaction

from base.view_utils import BaseMixin
from gdpr.models import Consent, ConsentFormSettings
from .forms import EnquiryForm
from .models import Enquiry, EnquiryError


class EnquiryCreateMixin:

    form_class = EnquiryForm
    model = Enquiry

    def _consent(self):
        return Consent.objects.get_consent(Enquiry.GDPR_CONTACT_SLUG)

    def _consent_form_settings(self):
        try:
            result = ConsentFormSettings.objects.get(
                slug=self.consent_form_settings
            )
            return result
        except ConsentFormSettings.DoesNotExist:
            raise EnquiryError(
                "Cannot find consent form settings: '{}'".format(
                    self.consent_form_settings
                )
            )

    def form_valid(self, form):
        """Do the form save within a transaction."""
        with transaction.atomic():
            result = super().form_valid(form)
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        consent = self._consent()
        # if we are not displaying the check box, then display the message
        consent_message = ""
        if not consent.show_checkbox:
            consent_message = consent.message
        if hasattr(self, "consent_form_settings"):
            consent_form_settings = self._consent_form_settings()
            context.update({"form_notice": consent_form_settings.form_notice})
        context.update({"consent_message": consent_message})
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(dict(consent=self._consent(), request=self.request))
        return kwargs

    def get_initial(self):
        """Returns the initial data to use for forms on this view."""
        result = super().get_initial()
        if self.request.user.is_authenticated:
            name = "{} {}".format(
                self.request.user.first_name, self.request.user.last_name
            )
            result.update(dict(email=self.request.user.email, name=name))
        return result


class EnquiryListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    paginate_by = 10
    model = Enquiry
