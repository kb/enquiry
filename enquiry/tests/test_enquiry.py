# -*- encoding: utf-8 -*-
import pytest

from enquiry.tests.factories import EnquiryFactory


@pytest.mark.django_db
def test_str():
    enquiry = EnquiryFactory(
        name="Patrick", email="test@pkimber.net", phone="01837"
    )
    assert "Patrick: test@pkimber.net, 01837" == str(enquiry)
