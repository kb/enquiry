# -*- encoding: utf-8 -*-
import csv
import pytest
import pytz

from datetime import datetime
from freezegun import freeze_time

from enquiry.reports import EnquiryMonthReport
from enquiry.tests.factories import EnquiryFactory
from login.tests.factories import UserFactory
from report.models import ReportSchedule, ReportSpecification


@pytest.mark.django_db
def test_enquiry_month():
    EnquiryMonthReport().init_report()
    freeze_date = datetime(2017, 5, 21, 18, 54, 12, tzinfo=pytz.utc)
    with freeze_time(freeze_date):
        # report data
        EnquiryFactory(
            name="Pat", description="Farm", email="pat@test.com", phone="01363"
        )
        EnquiryFactory(
            name="Jon", description="Baker", email="jon@test.com", phone="01837"
        )
        # schedule report
        report_specification = ReportSpecification.objects.get(
            slug=EnquiryMonthReport.REPORT_SLUG
        )
        parameters = {"year": 2017, "month": 5}
        report_specification.schedule(UserFactory(), parameters=parameters)
        # Run the report and find the schedule::
        schedule_pks = ReportSchedule.objects.process()
    assert 1 == len(schedule_pks)
    schedule_pk = schedule_pks[0]
    schedule = ReportSchedule.objects.get(pk=schedule_pk)
    # Check the report output::
    reader = csv.reader(open(schedule.output_file.path), "excel")
    first_row = None
    result = []
    for row in reader:
        if not first_row:
            first_row = row
        else:
            result.append(row)
    assert ["created", "name", "description", "email", "phone"] == first_row
    assert [
        ["21/05/2017 19:54:12", "Pat", "Farm", "pat@test.com", "01363"],
        ["21/05/2017 19:54:12", "Jon", "Baker", "jon@test.com", "01837"],
    ] == result
