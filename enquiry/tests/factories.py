# -*- encoding: utf-8 -*-
import factory

from enquiry.models import Enquiry


class EnquiryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Enquiry

    @factory.sequence
    def email(n):
        return "{:02d}@test.com".format(n)

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)

    @factory.sequence
    def phone(n):
        return "{:02d}".format(n)
