# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from enquiry.tests.factories import EnquiryFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_list(client):
    u = UserFactory(is_staff=True)
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    EnquiryFactory(name="a")
    EnquiryFactory(name="b")
    response = client.get(reverse("enquiry.list"))
    assert HTTPStatus.OK == response.status_code
    assert "enquiry_list" in response.context
    # order by created
    assert ["b", "a"] == [x.name for x in response.context["enquiry_list"]]
