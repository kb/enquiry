# -*- encoding: utf-8 -*-
import logging

from django import forms
from django.db import transaction
from django.urls import reverse
from django.utils import timezone
from django_recaptcha.fields import ReCaptchaField
from django_recaptcha.widgets import ReCaptchaV3

from base.form_utils import RequiredFieldForm
from gdpr.models import UserConsent
from mail.models import Notify
from mail.service import queue_mail_message
from mail.tasks import process_mail
from .models import Enquiry


logger = logging.getLogger(__name__)


class EnquiryForm(RequiredFieldForm):
    """user is not logged in... so we need a captcha."""

    captcha = ReCaptchaField(widget=ReCaptchaV3)
    consent_checkbox = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        """Don't use the captcha if the user is already logged in."""
        self.consent = kwargs.pop("consent")
        self.req = kwargs.pop("request")
        super().__init__(*args, **kwargs)
        for name in ("name", "email", "phone"):
            self.fields[name].widget.attrs.update({"class": "pure-input-1"})
        self.fields["description"].widget.attrs.update(
            {"class": "pure-input-1", "rows": 4}
        )
        self.fields["description"].label = "Message"
        if self.req.user.is_authenticated:
            del self.fields["captcha"]
        else:
            self.fields["captcha"].label = ""
        # gdpr
        if self.consent.show_checkbox:
            f = self.fields["consent_checkbox"]
            f.label = self.consent.message
        else:
            del self.fields["consent_checkbox"]

    class Meta:
        model = Enquiry
        fields = ("name", "description", "email", "phone")

    def _email_message(self, enquiry):
        result = "{} - enquiry received from {}, ".format(
            self._email_time(enquiry), enquiry.name
        )
        if enquiry.email:
            result = result + "{} ".format(enquiry.email)
        if enquiry.phone:
            result = result + "on {}".format(enquiry.phone)
        result = result + ":\n\n{}\n\n{}".format(
            enquiry.description,
            self.req.build_absolute_uri(reverse("enquiry.list")),
        )
        return result

    def _email_subject(self, instance):
        return "Enquiry from {}".format(instance.name)

    def _email_time(self, enquiry):
        x = timezone.localtime(enquiry.created)
        return x.strftime("%d/%m/%Y %H:%M")

    def clean_consent_checkbox(self):
        data = self.cleaned_data.get("consent_checkbox")
        if not data:
            raise forms.ValidationError(
                self.consent.no_consent_message,
                code="consent_checkbox__not_ticked",
            )
        return data

    def save(self, commit=True):
        instance = super().save(commit)
        if commit:
            email_addresses = [n.email for n in Notify.objects.all()]
            if email_addresses:
                queue_mail_message(
                    instance,
                    email_addresses,
                    self._email_subject(instance),
                    self._email_message(instance),
                )
                transaction.on_commit(lambda: process_mail.send())
            else:
                logging.error(
                    "Enquiry app cannot send email notifications.  "
                    "No email addresses set-up in 'mail.models.Notify'"
                )
            if self.req.user.is_authenticated:
                UserConsent.objects.set_consent(
                    self.consent, True, user=self.req.user
                )
            else:
                UserConsent.objects.set_consent(
                    self.consent, True, content_object=instance
                )
        return instance
