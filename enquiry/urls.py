# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import EnquiryListView

urlpatterns = [
    re_path(r"^$", view=EnquiryListView.as_view(), name="enquiry.list")
]
